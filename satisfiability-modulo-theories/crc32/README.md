## CRC32
I did not develop this code, I borrowed it from another project.

It generates a CRC32 of a specified length, for example `python3 ./crc32.py 0x1234ffaa 10` creates a 10 byte data string with a CRC32 of 0x1234ffaa.

Not this project requires Z3 and the python3 bindings.
