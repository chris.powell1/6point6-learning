#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char **argv, char **envp)
{
	// Test copy buffer - normally external source
	uint8_t copy_buffer[USHRT_MAX] = { 0x41 };

	// Set size in model checker
	uint32_t size;
	__CPROVER_input("size", size);

	printf("Using size %u\n", size);

	// Make sure size is less than equal to MAX USHORT (65,535)
	if(size >= USHRT_MAX) {
		printf("Error: size parameter too large\nABORTING!\n");
		return -1;
	}

	// Allocate space for buffer + 20 bytes for headers
	uint16_t data_size = size + 20;
        uint16_t *data = (uint16_t *) malloc(data_size);

	// Assume data_size (buffer allocation size) larger than input size
	__CPROVER_assert(data_size >= size, "data_size is too small");
	memcpy(data, copy_buffer, size);

	free(data);
}
