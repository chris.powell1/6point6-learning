#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

int main(int argc, char **argv, char **envp)
{
	// Test copy buffer - normally external source
	uint8_t copy_buffer[USHRT_MAX] = { 0x41 };

	// Check for argument
	if(argc != 2) {
		printf("Error: Provide exactly one argument\n");
		return -1;
	}

	// Get size from command line
	uint32_t size = atoi(argv[1]);
	printf("Using size %u\n", size);

	// Make sure size is less than equal to MAX USHORT (65,535)
	if(size >= USHRT_MAX) {
		printf("Error: size parameter too large\nABORTING!\n");
		return -1;
	}

	// Allocate space for buffer + 20 bytes for headers
	uint16_t data_size = size + 20;
        uint16_t *data = (uint16_t *) malloc(data_size);

	memcpy(data, copy_buffer, size);

	free(data);
}
