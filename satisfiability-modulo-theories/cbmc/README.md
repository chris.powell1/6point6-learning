## CBMC Testing
Run `./cbmc main-cmbc.c --function main --z3 --trace` for the main-cbmc.c file.

Run `make` for the binaries.
- main-normal has no exta protection and may not crash
- main-asan uses address sanitizer to demonstrate the vulnerabiliy
